--------------------------------------
-- 	Name : main.lua
--	Created by : Landon Gray
--  Date : 1/30/14
--------------------------------------
display.setStatusBar( display.HiddenStatusBar )

local background = display.newImageRect( "wood.jpg", display.contentWidth, display.contentHeight )	
background.x = display.contentWidth/2; background.y = display.contentHeight/2

local verticalScale = display.newRect( 0, 0, display.contentWidth/2, display.contentHeight )
verticalScale.x = display.contentWidth/2; verticalScale.y = display.contentHeight/2
verticalScale:setFillColor( 127/255, 255/255, 27/255)
verticalScale.alpha = 0.55

local lineWidth = display.contentWidth/2
local lineHeight = display.contentHeight/50

local topLine = display.newRect(0,0, lineWidth, lineHeight)
topLine.x = display.contentWidth/2; topLine.y = display.contentHeight*3/10
topLine:setFillColor( 255/255, 215/255, 0/255)

local bottomLine = display.newRect(0,0, lineWidth, lineHeight)
bottomLine.x = display.contentWidth/2; bottomLine.y = display.contentHeight*7/10
bottomLine:setFillColor( 255/255, 215/255, 0/255)

local bubble = display.newImage( "bubble.png" ,0, 0)
bubble:scale( .70, .70 )
bubble.x = display.contentWidth/2
bubble.y = display.contentHeight/2
bubble.alpha = .75

local levelText = display.newText(" ", display.contentWidth*9/10, display.contentHeight/2, native.systemFont, 50)

-- called when accelerometer movement detected
function onTilt(event)
	levelText.rotation = -90

	 -- move bubble if it is within the top and bottom of the screen
	if (event.yGravity > 0 or event.yGravity < display.contentHeight) then
		bubble.y = display.contentHeight/2 + (display.contentHeight/2 * event.yGravity )
	end

	-- display text if the bubble is level/not level (between the two lines)
	if (bubble.y > topLine.y and bubble.y < bottomLine.y ) then 
		levelText.text = "Level!"
		levelText:setFillColor(0, 255, 0)
	else 
		levelText.text = "Not Level!"
		levelText:setFillColor(255,0,0)
	end
end

-- still working on this function.
function onRotate(event)
	-- from page 542 of the “Beginning Mobile App Development with Corona.”
		radians = event.zRotation * event.deltaTime
		degrees = radians * (180 / math.pi)
		button.rotation = degrees
end

-- Add runtime listener
Runtime:addEventListener ("accelerometer", onTilt)
if system.hasEventSource("gyroscope") then
	Runtime:addEventListener("gyroscope", onRotate)
end
